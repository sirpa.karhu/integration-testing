const pad = (hex) => {
    return(hex.length === 1 ? "0" + hex : hex );
}

module.exports = {
    rgbToHex: (red, green, blue) => {
        const redHex = red.toString(16);
        const greenHex = green.toString(16);
        const blueHex = blue.toString(16);

        return pad(redHex) + pad(greenHex) + pad(blueHex); // "ff0000"
    }, 
    hexToRGB: (hex) => {
        const redH = hex.slice(0,2);
        const greenH = hex.slice(2,4);
        const blueH =hex.toString(16).substr(-2);

        const red = parseInt(redH, 16);
        const green = parseInt(greenH, 16);
        const blue = parseInt(blueH, 16);

        return "("+ red +", " +green + ", "+blue + ")"
    }
}